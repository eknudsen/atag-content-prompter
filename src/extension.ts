// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import {window, workspace, commands, Disposable, ExtensionContext, StatusBarAlignment, StatusBarItem, TextDocument} from 'vscode';

// this method is called when your extension is activated. activation is
// controlled by the activation events defined in package.json
export function activate(ctx: ExtensionContext) 
{

    // Use the console to output diagnostic information (console.log) and errors (console.error)
    // This line of code will only be executed once when your extension is activated
    // console.log('All authored content must conform to Level A and Level AA Success Criteria and Conformance Requirements in the Web Content Accessibility Guidelines (WCAG) version 2.0.');

    // window.showInformationMessage('All authored content must conform to Level A and Level AA Success Criteria and Conformance Requirements in the Web Content Accessibility Guidelines (WCAG) version 2.0.');

    let controller = new ComplianceController();
    ctx.subscriptions.push(controller);
}

class ComplianceController 
{
    private _disposable: Disposable;

    constructor()
    {
        let subscriptions: Disposable[] = [];
        workspace.onWillSaveTextDocument(this._onEvent, this, subscriptions);
        workspace.onDidOpenTextDocument(this._onEvent, this, subscriptions);
    }

    private _onEvent() 
    {
        // let editor = window.activeTextEditor;
        // if (!editor) 
        // {
        //     return;
        // }

        // let doc = editor.document;

        // if (doc.languageId === "markdown" || doc.languageId === "html") 
        // {
            window.showInformationMessage('All authored content must conform to Level A and Level AA Success Criteria and Conformance Requirements in the Web Content Accessibility Guidelines (WCAG) version 2.0.');
        // }
    }

    public dispose() {
        this._disposable.dispose();
    }
}
